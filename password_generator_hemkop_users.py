#!/bin/env python3

import os
import sys
import random
import string
import codecs

def generatePassword():
    letters = string.ascii_letters + string.digits + string.digits
    passwordLength = 8
    password = ''.join(random.choice(letters) for i in range(passwordLength))
    if (any(i.isdigit() for i in password)):
        if (any(i.isupper() for i in password)):
            if (any(i.islower() for i in password)):
                return password
    return generatePassword()

with open(sys.argv[1]) as fp:

    fileName = os.path.basename(fp.name)
    folderName = os.path.realpath(fp.name).replace(fileName,'')
    fileName = fileName.split('_')[0]
    if not os.path.exists(folderName + "CMS_generated_files/"):
        os.makedirs(folderName + "CMS_generated_files/")
    if not os.path.exists(folderName + "vs_import/"):
        os.makedirs(folderName + "vs_import/")
    if not os.path.exists(folderName + "failed_users/"):
        os.makedirs(folderName + "failed_users/")

    if os.path.isfile(folderName + "CMS_generated_files/" + fileName + "_generated_for_CMS.csv"):
        variable = input('Files already exist do you want to overwrite? y/n ')
        if variable != "y":
            print("Script canceled!!!")
            quit()
        else:
            print("Continues and will overwrite files!!!")

    file = codecs.open(folderName + "CMS_generated_files/" + fileName + "_generated_for_CMS.csv", "w", "utf-8")
    file2 = codecs.open(folderName + "vs_import/" + fileName + "_intern_w_password.csv", "w", "utf-8")
    file3 = codecs.open(folderName + "failed_users/" + fileName + "_incorrect_users.csv", "w", "utf-8")
    for cnt, line in enumerate(fp):
        if cnt == 0:
            file.write("customerName,socialSecurity,memberNumber,password\n")
            file2.write("memberid,password\n")
        else:
            name, socialId, memberId  = line.replace(';',',').strip().split(',')
            socialId = socialId.replace('-','')
            if len(socialId) == 10 or len(socialId) == 12 and len(memberId) == 16:
                password = generatePassword()
                file.write(line.strip() + "," + password + "\n")
                file2.write(memberId + "," + password  + "\n")
            else:
                file3.write(line.strip() + "\n")
    file.close
    file2.close
    file3.close
